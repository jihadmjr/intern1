import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Problem3 {
	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	static ArrayList<String> ans = new ArrayList<String>();


	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String input = in.readLine();
		
		input = input.substring(input.indexOf("+")+1);
		input = input.replaceAll("\\(.*?\\) ?", "");
		input = input.replace(".","");
		input = input.replace("/","");
		input = input.replace("-","");
		System.out.print(input);

	}

}
