import java.util.*;
 
public class Problem5
{
    static int findBus(int arr[], int dep[], int n)
    {
    	Arrays.sort(arr);
        Arrays.sort(dep);
        
        int plat_needed = 1, result = 1;
        int i = 1, j = 0;
 
        while (i < n && j < n){
        	
            if (arr[i] < dep[j]){
                plat_needed++;
                i++;
                if (plat_needed > result) {
                	result = plat_needed;
                }
            }
            else {
            	
                plat_needed--;
                j++;
            }
        }
 
    return result;
    }
 
    // Driver programs
    public static void main(String args[])
    {
    	int arr[] = {1000, 1005, 1010, 1025, 1045};
    	int dep[] = {1005, 1015, 1030, 1040, 1050};
        
    	int n = arr.length;
        System.out.println("Maximum Number of" + " Bus = " + findBus(arr, dep, n));
    }
}
